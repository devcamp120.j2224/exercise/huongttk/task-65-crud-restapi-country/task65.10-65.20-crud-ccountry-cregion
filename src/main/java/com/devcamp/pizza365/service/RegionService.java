package com.devcamp.pizza365.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;

@Service
public class RegionService {
    // -------------IMPOTR INTERFACE--------------
    @Autowired
    CountryRepository countryRepository;
    @Autowired
    RegionRepository regionRepository;

    // -------------GET---------------------
    /*
     * // Cách1: viết tại service gọi bên controller
     * public ResponseEntity<List<CRegion>> getAllRegion() {
     * try {
     * List<CRegion> listRegion = new ArrayList<CRegion>();
     * regionRepository.findAll().forEach(listRegion::add);
     * return new ResponseEntity<>(listRegion, HttpStatus.OK);
     * } catch (Exception ex) {
     * return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
     * }
     * }
     * 
     * // Cách2: viết getAll bên controller(vì ngắn)
     * 
     * @CrossOrigin
     * 
     * @GetMapping("/region/all")
     * public List<CRegion> getAllRegion() {
     * return regionRepository.findAll();
     * }
     */

    public ResponseEntity<Object> getRegionById(Long id) {
        Optional<CRegion> regionData = regionRepository.findById(id);
        if (regionData.isPresent()) {
            return new ResponseEntity<>(regionData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // GET REGION BY COUNTRY ID
/* 
    public ResponseEntity<Object> getRegionByCountryId(Long countryId) {
        Optional<CCountry> albumData = countryRepository.findById(countryId);
		if (albumData.isPresent())
			return new ResponseEntity<>(albumData.get().getRegions(), HttpStatus.OK);
		else
			return new ResponseEntity<>("Album not found", HttpStatus.NOT_FOUND);
    }
*/
    // -------------------------POST-----------------------

    public ResponseEntity<Object> createRegion(Long countryId, CRegion cRegion) {
        try {
            Optional<CCountry> countryData = countryRepository.findById(countryId);
            if (countryData.isPresent()) {
                CRegion newRole = new CRegion();
                newRole.setRegionName(cRegion.getRegionName());
                newRole.setRegionCode(cRegion.getRegionCode());
                newRole.setCountry(cRegion.getCountry());

                CCountry _country = countryData.get();
                newRole.setCountry(_country);
                newRole.setCountryName(_country.getCountryName());

                CRegion savedRole = regionRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // ------------- PUT--------------------

    public ResponseEntity<Object> updateRegionById(Long id, CRegion cRegion) {
        try {
            Optional<CRegion> regionData = regionRepository.findById(id);
            if (regionData.isPresent()) {
                CRegion newRegion = regionData.get();
                newRegion.setRegionName(cRegion.getRegionName());
                newRegion.setRegionCode(cRegion.getRegionCode());
                CRegion savedRegion = regionRepository.save(newRegion);
                return new ResponseEntity<>(savedRegion, HttpStatus.OK);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Image: " + e.getCause().getCause().getMessage());
        }

        return new ResponseEntity<>("Image not found", HttpStatus.NOT_FOUND);

    }

    //////////////////////////// DELETE SERVICES///////////////////////////
    public ResponseEntity<Object> deleteRegionById(Long id) {
        try {
            regionRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> deleteAllRegion() {
        try {
            regionRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
