package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.RegionRepository;
import com.devcamp.pizza365.service.RegionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/region")
public class RegionController {
	@Autowired
	RegionService regionService;
	@Autowired
	RegionRepository regionRepository;



	/////////////////////// GET ////////////////////
	@GetMapping()
    public List<CRegion> getAllRegions() {
        return regionRepository.findAll();
    }


	@GetMapping("{id}")
	public ResponseEntity<Object> getRegionById(@PathVariable("id") Long id) {
		ResponseEntity<Object> respon = regionService.getRegionById(id);
		return respon;
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}")
    public List < CRegion > getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
        return regionRepository.findByCountryId(countryId);
    }
/* 
	@GetMapping("/country/{countryId}/regions/{id}")
	public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,
			@PathVariable(value = "id") Long regionId) {
		return regionRepository.findByIdAndCountryId(regionId, countryId);
	}
*/
	// ----------------------- POST --------------------------------------

	@PostMapping()
	public ResponseEntity<Object> createRegion(@RequestParam(value = "countryId") Long countryId,@RequestBody CRegion cRegion) {
		ResponseEntity<Object> respon = regionService.createRegion(countryId, cRegion);
		return respon;
	}


	//--------------------PUT------------------------------------

	@PutMapping("{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		ResponseEntity<Object> respon = regionService.updateRegionById(id, cRegion);
		return respon;
	}


	//----------------------DELETE---------------------------

	@DeleteMapping("{id}")
    public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
        ResponseEntity<Object> respon = regionService.deleteRegionById(id);
        return respon;
    }
    
    @DeleteMapping()
    public ResponseEntity<Object> deleteAllRegion() {
        ResponseEntity<Object> respon = regionService.deleteAllRegion();
        return respon;
    }
}
